//http://www.vogella.com/tutorials/JavaAlgorithmsDijkstra/article.html
//find all distancea put into massiiv and find the shortest ones out of it
import com.sun.org.apache.bcel.internal.generic.RETURN;
//https://www.geeksforgeeks.org/floyd-warshall-algorithm-dp-16/
//https://www.baeldung.com/java-dijkstra
//https://stackoverflow.com/questions/1066589/iterate-through-a-hashmap
//https://en.wikipedia.org/wiki/Vertex_(geometry)
import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      Graph g = new Graph ("G");
      g.createRandomSimpleGraph (9, 15);
      System.out.println (g);

      int[][] matrix = g.createAdjMatrix();
      // Print the solution
      // TODO!!! Your experiments here
//      List<Integer> allPathLengthList = new LinkedList<>();
//      List<Integer> longestPathLengthList = new LinkedList<>();

      Map<Integer,Integer> allPathLengthList = new HashMap<>();
      Map<Integer,Integer> longestPathLengthList = new HashMap<>();

      for (int i = 0; i < matrix.length; i++){
         for (int j = 0; j < matrix.length;  j++){
            if (i != j && matrix[i][j] == 0) {
               matrix[i][j] = 9999;
            }
         }
      }

      g.floydWarshall(matrix);
      System.out.println("\nThe table of matrix connections and pathes" + "\n" +
              Arrays.deepToString(g.createAdjMatrix()) + "\n");
   }

   // TODO!!! add javadoc relevant to your problem
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      // You can add more fields, if needed

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      // TODO!!! Your Vertex methods here!

   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      // You can add more fields, if needed

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      // TODO!!! Your Arc methods here!
   } 


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      // You can add more fields, if needed

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      // TODO!!! Your Graph methods here! Probably your solution belongs here.
         final static int INF = 99999, V = 9;

         void floydWarshall(int graph[][])
         {
            int dist[][] = new int[V][V];
            int i, j, k;

        /* Initialize the solution matrix same as input graph matrix.
           Or we can say the initial values of shortest distances
           are based on shortest paths considering no intermediate
           vertex. */
            for (i = 0; i < V; i++)
               for (j = 0; j < V; j++)
                  dist[i][j] = graph[i][j];

        /* Add all vertices one by one to the set of intermediate
           vertices.
          ---> Before start of an iteration, we have shortest
               distances between all pairs of vertices such that
               the shortest distances consider only the vertices in
               set {0, 1, 2, .. k-1} as intermediate vertices.
          ----> After the end of an iteration, vertex no. k is added
                to the set of intermediate vertices and the set
                becomes {0, 1, 2, .. k} */
            for (k = 0; k < V; k++)
            {
               // Pick all vertices as source one by one
               for (i = 0; i < V; i++)
               {
                  // Pick all vertices as destination for the
                  // above picked source
                  for (j = 0; j < V; j++)
                  {
                     // If vertex k is on the shortest path from
                     // i to j, then update the value of dist[i][j]
                     if (dist[i][k] + dist[k][j] < dist[i][j])
                        dist[i][j] = dist[i][k] + dist[k][j];
                  }
               }
            }

            // Print the shortest distance matrix
            printSolution(dist);
         }

         void printSolution(int dist[][])
         {
            Map<Vertex,Integer> longestPathLengthList = new HashMap<>();
            int sizeFirstDemension = dist.length;
            int sizeSecondDemension = dist[0].length;
            int values = INF;
            Vertex current = this.first;
            String keskpunkt = "The unique center is located at: ";
            for (int i=0; i<sizeFirstDemension; i++){
               int biggest = 0;
               for (int j=0; j<sizeSecondDemension; j++){
                  if (biggest < dist[i][j]){
                     biggest = dist[i][j];

                  }
               }
               longestPathLengthList.put(current,biggest);
               current = current.next;
            }
            System.out.println("The following matrix shows the shortest "+
                    "distances between every pair of vertices");
            for (int i=0; i<V; ++i)
            {
               for (int j=0; j<V; ++j)
               {
                  if (dist[i][j]==INF)
                     System.out.print("INF ");
                  else
                     System.out.print(dist[i][j]+"   ");
               }
               System.out.println();
            }
            System.out.println("The longest pathes of all nodes"+"\n"+longestPathLengthList);
            for(Map.Entry<Vertex, Integer> entry : longestPathLengthList.entrySet()){
               if (values > entry.getValue()){
                  values = entry.getValue();
               }
            }
            for (Map.Entry<Vertex, Integer> entry : longestPathLengthList.entrySet()){
               if (values == entry.getValue()){
                  keskpunkt += entry.getKey().toString() + " ";
               }
            }
            System.out.println(keskpunkt);

         }

   }
}